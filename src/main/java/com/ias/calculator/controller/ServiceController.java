package com.ias.calculator.controller;

import com.ias.calculator.model.ServiceModel;
import com.ias.calculator.repository.ServiceRepository;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping(value = "/service")
public class ServiceController {
    private final ServiceRepository repository;

    public ServiceController(ServiceRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/{id}")
    public Mono<ServiceModel> get(@PathVariable("id") Integer id) {
        return this.repository.findById(id);
    }

    @GetMapping
    public Flux<ServiceModel> getAll() {
        return this.repository.findAll();
    }

    @PostMapping
    public Mono<ServiceModel> create(@RequestBody ServiceModel service) {
        return this.repository.save(service);
    }

    @PutMapping("/{id}")
    public Mono<ServiceModel> update(@PathVariable("id") Integer id, @RequestBody ServiceModel service){
        return this.repository.findById(id)
                .map(s -> {
                    s.setTechnicianId(service.getTechnicianId());
                    s.setServiceName(service.getServiceName());
                    s.setInitialDate(service.getInitialDate());
                    s.setFinalDate(service.getFinalDate());

                    return s;
                })
                .flatMap(s -> this.repository.save(s));
    }

    @DeleteMapping("/{id}")
    public Mono<Void> delete(@PathVariable("id") Integer id){
        return this.repository.deleteById(id);
    }

    @DeleteMapping
    public Mono<Void> deleteAll() {
        return this.repository.deleteAll();
    }

    @GetMapping("/technician_id/{technicianId}")
    public Flux<ServiceModel> getByTechnicianId(@PathVariable("technicianId") Integer technicianId){
        return this.repository.findByTechnicianId(technicianId);
    }

    @GetMapping("/service_name/{serviceName}")
    public Flux<ServiceModel> getByServiceName(@PathVariable("serviceName") String serviceName){
        return this.repository.findByServiceNameContaining(serviceName);
    }
}
