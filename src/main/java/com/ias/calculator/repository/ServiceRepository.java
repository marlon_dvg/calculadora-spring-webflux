package com.ias.calculator.repository;

import com.ias.calculator.model.ServiceModel;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface ServiceRepository extends ReactiveCrudRepository<ServiceModel,Integer> {

    @Query("SELECT * FROM service WHERE technician_id = @technicianId")
    Flux<ServiceModel> findByTechnicianId(Integer technicianId);

    @Query("SELECT * FROM service WHERE service_name LIKE '%' + @serviceName + '%'")
    Flux<ServiceModel> findByServiceNameContaining(String serviceName);
}
